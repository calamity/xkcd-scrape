import unittest
from xkcdscrape import xkcd


class Tests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Tests, self).__init__(*args, **kwargs)  # that's a mess
        self.archive = xkcd.parseArchive()

    def test_for_latest(self):
        info = xkcd.getComicInfo(archive=self.archive)
        self.assertIsInstance(info, dict)

    def test_for_link(self):
        info = xkcd.getComicInfo("https://xkcd.com/2000", self.archive)
        self.assertIsInstance(info, dict)
        self.assertEqual(info['num'], '2000')

    def test_for_link_no_https(self):
        info = xkcd.getComicInfo("xkcd.com/2000", self.archive)
        self.assertIsInstance(info, dict)
        self.assertEqual(info['num'], '2000')

    def test_for_string_num(self):
        info = xkcd.getComicInfo("2000", self.archive)
        self.assertIsInstance(info, dict)
        self.assertEqual(info['num'], '2000')

    def test_for_int(self):
        info = xkcd.getComicInfo(2000, self.archive)
        self.assertIsInstance(info, dict)
        self.assertIn("date", info)
        self.assertEqual(info['num'], '2000')

    def test_archiveless(self):
        info = xkcd.getComicInfo(2000)
        self.assertIsInstance(info, dict)
        self.assertNotIn("date", info)
        self.assertEqual(info['num'], '2000')

if __name__ == "__main__":
    unittest.main()
