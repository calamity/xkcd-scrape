import unittest
from xkcdscrape import xkcd


class Tests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Tests, self).__init__(*args, **kwargs)  # that's a mess
        self.archive = xkcd.parseArchive()

    def test_for_random(self):
        info = xkcd.getRandomComic(self.archive)
        self.assertIsInstance(info, dict)

    def test_for_random_fromarchive(self):
        info = xkcd.getRandomComic(self.archive, True)
        self.assertIsInstance(info, dict)
        self.assertIn("date", info)

    def test_for_random_archiveless(self):
        info = xkcd.getRandomComic(None)
        self.assertIsInstance(info, dict)
        self.assertNotIn("date", info)


if __name__ == "__main__":
    unittest.main()
